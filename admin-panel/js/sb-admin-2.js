(function($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on("click", function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $(".sidebar .collapse").collapse("hide");
    }
  });
  $("#night").on("click", function(e) {
    $("#card").toggleClass("bg-white fade-in");
    $("#card").toggleClass("bg-dark fade-in");
    $("#content").toggleClass("bg-dark fade-in");
    $("#dark-footer").toggleClass("bg-white fade-in");
    $("#dark-footer").toggleClass("bg-dark ");
    $("#dark-nav").toggleClass("bg-white fade-in");
    $("#dark-nav").toggleClass("bg-dark-nav fade-in");
    $("#route-body").toggleClass("bg-white fade-in");
    $("#route-body").toggleClass("bg-dark fade-in");
    $("#envelope-btn").toggleClass("text-primary");
    $("#envelope-btn").css("color", "white");
    $("#bell-btn").toggleClass("text-primary");
    $("#route-table").toggleClass("table-dark");
    $("#route-heading i").toggleClass("fa-moon");
    $("#route-heading i").toggleClass("fa-sun");
    $("#sidebarToggleTop").toggleClass("color-white");
    // $("#login-btn").css("color", "white");
    if ($("#route-heading").hasClass("bg-gradient-primary")) {
      $("#route-heading").removeClass("bg-gradient-primary");
      $("#route-heading").addClass("bg-dark-nav");
    }
    // $("#Arrival-table").removeClass("d-none");
    $("#bell-btn").css("color", "white");
    $("#login-btn").toggleClass("color-white");
    // $("#login-btn").toggleClass("color-black");
  });
  $("#bus-on-road").on("click", function(e) {
    $("#Arrival-table").removeClass("d-none");
    $("#total-bus-table").addClass("d-none");
    $("#departure-bus-table").addClass("d-none");
    $("#maintain-bus-table").addClass("d-none");
  });
  $("#total-bus").on("click", function(e) {
    $("#total-bus-table").removeClass("d-none");
    $("#Arrival-table").addClass("d-none");
    $("#departure-bus-table").addClass("d-none");
    $("#maintain-bus-table").addClass("d-none");
  });
  $("#bus-on-station").on("click", function(e) {
    $("#departure-bus-table").removeClass("d-none");
    $("#Arrival-table").addClass("d-none");
    $("#maintain-bus-table").addClass("d-none");
    $("#total-bus-table").addClass("d-none");
  });
  $("#bus-maintain").on("click", function(e) {
    $("#maintain-bus-table").removeClass("d-none");
    $("#total-bus-table").addClass("d-none");
    $("#Arrival-table").addClass("d-none");
    $("#departure-bus-table").addClass("d-none");
  });
  $("#route-submit").on("click", function(e) {
    $("#route-table").removeClass("d-none");
    $("#route-table").addClass("mt-4");
  });
  $(".showmore").on("click", function(e) {
    var { id } = e.target;
    console.log(id);
    window.location.assign("/admin-panel/BusDetail.html?id=" + id);
  });
  /*clock*/
  var $dOut = $("#date"),
    $hOut = $("#hours"),
    $mOut = $("#minutes"),
    $sOut = $("#seconds"),
    $ampmOut = $("#ampm");
  var months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  var days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];

  function update() {
    var date = new Date();

    var ampm = date.getHours() < 12 ? "AM" : "PM";

    var hours =
      date.getHours() == 0
        ? 12
        : date.getHours() > 12
        ? date.getHours() - 12
        : date.getHours();

    var minutes =
      date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();

    var seconds =
      date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

    var dayOfWeek = days[date.getDay()];
    var month = months[date.getMonth()];
    var day = date.getDate();
    var year = date.getFullYear();

    var dateString = dayOfWeek + ", " + month + " " + day + ", " + year;

    $dOut.text(dateString);
    $hOut.text(hours);
    $mOut.text(minutes);
    $sOut.text(seconds);
    $ampmOut.text(ampm);
  }

  update();
  window.setInterval(update, 1000);

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $(".sidebar .collapse").collapse("hide");
    }
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $("body.fixed-nav .sidebar").on("mousewheel DOMMouseScroll wheel", function(
    e
  ) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on("scroll", function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $(".scroll-to-top").fadeIn();
    } else {
      $(".scroll-to-top").fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on("click", "a.scroll-to-top", function(e) {
    var $anchor = $(this);
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $($anchor.attr("href")).offset().top
        },
        1000,
        "easeInOutExpo"
      );
    e.preventDefault();
  });
})(jQuery); // End of use strict
