axios.get("../../data/Routes.json").then(
  function(response) {
    routeId = 420;
    response.data.map(item => {
      const { id, path, time, reg_id, delay, status, bus_type } = item;
      if (id === routeId) {
        for (i = 0; i < path.length; i++) {
          var list = "<li";
          if (i === status) {
            list += " class='is-active'";
          }
          list +=
            "><span class='place m-1 p-2'>" +
            path[i] +
            "</span></br><span>" +
            time[i] +
            "</span>";
          if (delay > 20 && i === status) {
            list +=
              "<span class='badge badge-danger m-1'>Delay: " +
              delay +
              " Minutes</span></li>";
          } else if (delay <= 20 && i === status && delay !== 0) {
            list +=
              "<span class='badge badge-warning m-1'>Delay: " +
              delay +
              " Minutes</span></li>";
          } else if (i === status) {
            list +=
              "<span class='badge badge-success m-1'>No Delay</span></li>";
          }
          $(".multi-steps").append(list);
        }
        $("#routeId").append(id);
        $("#regNo").append(reg_id);
        $("#busType").append(bus_type);
      }
    });
  },
  function(err) {
    console.log(err);
  }
);
