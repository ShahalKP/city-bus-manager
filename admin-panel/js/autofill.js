axios.get("../../data/Stations.json").then(
  function(response) {
    $("#from-input").autocomplete({
      lookup: response.data
    });
    $("#to-input").autocomplete({
      lookup: response.data
    });
  },
  function(err) {
    console.log(err);
  }
);
