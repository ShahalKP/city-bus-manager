$("#route-submit").click(e => {
  axios.get("../../data/Routes.json").then(
    function(response) {
      to = $("#to-input")[0].value.toLowerCase();
      from = $("#from-input")[0].value.toLowerCase();
      var str = "";
      response.data.map(item => {
        const { path, time, reg_id, bus_type } = item;
        if (
          from === path[0].toLowerCase() &&
          to === path[path.length - 1].toLowerCase()
        ) {
          console.log(path);
          str += `<tr><td>${reg_id}</td><td>${path[0]}</td><td>${
            time[0]
          }</td><td>${path[path.length - 1]}</td><td>${
            time[time.length - 1]
          }</td><td>${bus_type}</td>`;
        }
      });
      $(".table-body").append(str);
    },
    function(err) {
      console.log(err);
    }
  );
});
